# obs_meta

### 介绍
该项目用于管理obs系统多个仓库的软件包，通过_service文件达到对obs仓库中软件包的增、删、改、查目的。

### 软件目录结构
```d
obs_meta
    master                    软件包码云仓库主分支
        openeuler:Mainline    obs系统openeuler:Mainline仓库，openeuler主线软件包所在仓库
        openeuler:Factory     obs系统openeuler:Factory仓库，openeuler软件包验证仓库
        openeuler:Extras
		openeuler:Eopl
        bringInRely
    openEuler-20.03-LTS
    openEuler-EPOL-LTS
```
	重点请关注openeuler:Mainline和openeuler:Factory两个目录，
	新增的openeuler软件包必须先加到obs_meta/master/openeuler:Factory目录，加入之后会在obs系统的openeuler:Factory仓库创建相应的软件包，
	软件包通过obs系统openeuler:Factory仓库的编译验证成功后，软件包才可以被移动到obs_meta/master/openeuler:Mainline目录，即纳入openeuler主线仓库。


### 使用说明
##### 新增软件包

在fork出来的个人仓库 obs_meta 仓库的 master/openeuler:Factory 目录下创建 "<软件包名>/_service" 文件内容参照实例如下：
	```xml
		<services>
			<service name="tar_scm_kernel_repo">
				<param name="scm">软件包名</param>
				<param name="url">next/openEuler/软件包名</param>
			</service>
		</services>
	```

提交pr

##### 移动软件包
> **常用于将obs系统里 openEuler:Factory 仓库编译成功的包，移动到 openEuler:Mainline 仓库**

	在个人码云仓库 obs_meta 仓库下， 将 "<软件包名>" 目录从当前目录下如 master/openEuler:Factory 目录移动到新的目录下如 master/openEuler:Mainline 目录下
	通过个人码云仓库 obs_meta 向 obs_meta 主仓库提交pr，等待pr被接受
	pr被接受后，后台会删除obs系统原仓库的包即openEuler:Factory仓库里对应的软件包，在新的仓库如openEuler:Mainline中创建对应的软件包

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request



